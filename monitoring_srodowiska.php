<?php
require_once 'includes/twigAutoloader.php';

$siteId = "monitoring";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/monitoring_srodowiska.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        "picture" => $pictureTitle,
        "alt" => "Monitoring Środowiska",
    )
);