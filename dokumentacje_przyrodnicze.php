<?php
require_once 'includes/twigAutoloader.php';

$siteId = "docs";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/dokumentacje_przyrodnicze.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        "picture" => $pictureTitle,
        "alt" => "Dokumenty przyrodnicze, ochrona środowiska",
    )
);