<?php
require_once 'includes/twigAutoloader.php';

$template = $twig->loadTemplate('o_nas.html.twig');
echo $template->render(array(
    "menu" => array(
        "aboutUs" => "active"
    )
));
