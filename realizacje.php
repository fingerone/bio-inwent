<?php
require_once 'includes/twigAutoloader.php';

$template = $twig->loadTemplate('realizacje.html.twig');
echo $template->render(array(
    "menu" => array(
        "projects" => "active"
    )
));
