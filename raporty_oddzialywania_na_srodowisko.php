<?php
require_once 'includes/twigAutoloader.php';

$siteId = "raports";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/raporty_oddzialywania_na_srodowisko.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        //"picture" => $pictureTitle,
        "alt" => "Inwentaryzacje Przyrodnicze, Inwentarycje Dendrologiczne",
    )
);