<?php
require_once 'includes/twigAutoloader.php';

$template = $twig->loadTemplate('baza_danych.html.twig');
echo $template->render(array(
    "menu" => array(
        "database" => "active"
    )
));