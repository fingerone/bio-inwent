<?php
require_once 'includes/twigAutoloader.php';

$template = $twig->loadTemplate('home.html.twig');
echo $template->render(array(
    "menu" => array(
        "home" => "active"
    ),
));