<?php
require_once 'includes/twigAutoloader.php';

$template = $twig->loadTemplate('eksperci.html.twig');
echo $template->render(array(
    "menu" => array(
        "team" => "active"
    ),
));
