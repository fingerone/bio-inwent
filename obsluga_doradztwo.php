<?php
require_once 'includes/twigAutoloader.php';

$siteId = "advice";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/obsluga_doradztwo.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        "picture" => $pictureTitle,
        "alt" => "Obsługa i doradztwo - Ochrona Środowiska",
    )
);