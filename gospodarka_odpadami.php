<?php
require_once 'includes/twigAutoloader.php';

$siteId = "waste-managment";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/gospodarka_odpadami.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        "picture" => $pictureTitle,
        "alt" => "Gospodarka odpadami",
    )
);