<?php
require_once 'includes/twigAutoloader.php';

$siteId = "environmental-expertises";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/ekspertyzy_srodowiskowe.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        //"picture" => $pictureTitle,
        "alt" => "Inwentaryzacje Przyrodnicze, Inwentarycje Dendrologiczne",
    )
);