<?php
require_once 'includes/twigAutoloader.php';

$siteId = "dendrological-inventory";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/inwentaryzacja_dendrologiczna.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        //"picture" => $pictureTitle,
        "alt" => "Inwentaryzacje Przyrodnicze, Inwentarycje Dendrologiczne",
    )
);