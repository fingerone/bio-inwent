<?php
require_once 'includes/twigAutoloader.php';

$template = $twig->loadTemplate('aktualnosci.html.twig');
echo $template->render(array(
    "menu" => array(
        "news" => "active"
    )
));
