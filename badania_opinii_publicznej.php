<?php
require_once 'includes/twigAutoloader.php';

$siteId = "polls";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/badania_opinii_publicznej.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        "picture" => $pictureTitle,
        "alt" => "Badania opinii publicznej - Ochrona środowiska",
    )
);