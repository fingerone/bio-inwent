<?php
require_once 'includes/twigAutoloader.php';

$template = $twig->loadTemplate('oferta_lista.html.twig');
echo $template->render(array(
    "menu" => array(
        "offer" => "active"
    ),
));
