<?php
require_once 'includes/twigAutoloader.php';

$template = $twig->loadTemplate('kontakt.html.twig');
echo $template->render(array(
    "menu" => array(
        "contact" => "active"
    )
));