<?php
require_once 'includes/twigAutoloader.php';

$siteId = "service";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/kompleksowa_obsluga_stacji_demontazu_pojazdow.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        //"picture" => $pictureTitle,
        "alt" => "Badania opinii publicznej - Ochrona środowiska",
    )
);