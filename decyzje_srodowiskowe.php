<?php
require_once 'includes/twigAutoloader.php';

$siteId = "environmental-decisions";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/decyzje_srodowiskowe.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        //"picture" => $pictureTitle,
        "alt" => "Inwentaryzacje Przyrodnicze, Inwentarycje Dendrologiczne",
    )
);