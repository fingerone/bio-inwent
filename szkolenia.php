<?php
require_once 'includes/twigAutoloader.php';

$siteId = "trainings";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/szkolenia.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        "picture" => $pictureTitle,
        "alt" => "Szkolenia - inwentaryzacja przyrodnicza, ocena środowiskowa",
    )
);