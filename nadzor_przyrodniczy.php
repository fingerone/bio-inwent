<?php
require_once 'includes/twigAutoloader.php';

$siteId = "environmental-supervision";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/nadzor_przyrodniczy.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        //"picture" => $pictureTitle,
        "alt" => "Inwentaryzacje Przyrodnicze, Inwentarycje Dendrologiczne",
    )
);