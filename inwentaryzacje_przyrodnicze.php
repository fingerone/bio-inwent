<?php
require_once 'includes/twigAutoloader.php';

$siteId = "stocktaking";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/inwentaryzacje_przyrodnicze.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        "picture" => $pictureTitle,
        "alt" => "Inwentaryzacje Przyrodnicze, Inwentarycje Dendrologiczne",
    )
);