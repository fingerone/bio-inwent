<?php
require_once 'includes/twigAutoloader.php';

$siteId = "information_cards";
$pictureTitle = $siteId . ".jpg";

$template = $twig->loadTemplate('oferta/karty_informacyjne_przedsiewziec.html.twig');

echo $template->render(
    array(
        "menu" => array(
            "offer" => "active"
        ),
        "id" => $siteId,
        //"picture" => $pictureTitle,
        "alt" => "Inwentaryzacje Przyrodnicze, Inwentarycje Dendrologiczne",
    )
);